'use strict';

const fs = require( 'fs' );
const mariadb = require( 'mariadb' );
const logger = require( './pipeline-logger.js' ).getPipelineLogger();

/**
 * Check if the expected database exists
 *
 * @param {Pool} pool
 * @param {string} dbName
 * @return {Promise<boolean>}
 */
async function dbExists( pool, dbName ) {
	const connection = await pool.getConnection();
	let dbCheck = await connection.query( 'SHOW DATABASES LIKE ?', dbName );
	dbCheck = dbCheck[ 0 ];
	await connection.end();
	await pool.end();
	return !!dbCheck;
}

/**
 * Get the statements in a SQL file and process them one at a time.
 *
 * @param {string} name
 * @param {string} description
 * @param {string} schema filepath to sql file
 * @param {Pool} ipoidDbPool
 * @return {boolean} false if the update was already run, true if a new update was run.
 */
async function updateSchema( name, description, schema, ipoidDbPool ) {
	const connection = await ipoidDbPool.getConnection();

	// Check that the update hasn't been run yet
	let updateRun = await connection.query( 'SELECT timestamp FROM update_log WHERE update_name = ?', [ name ] );
	updateRun = updateRun[ 0 ] ? updateRun[ 0 ].timestamp : false;
	if ( updateRun ) {
		logger.info( `Update ${ name } already run, skipping...` );
		await connection.end();
		return false;
	}

	logger.info( `Running db update "${ name }" that ${ description }...` );

	// Create a separate statement for a block separated by each empty line.
	const schemaStatements = schema.split( /\n\s*\n/ );
	for ( const statement of schemaStatements ) {
		await connection.query( statement );
	}
	await connection.query( 'INSERT INTO update_log (update_name) VALUES (?)', [ name ] );
	await connection.end();
	return true;
}

/**
 * Return a new database pool using env variables.
 *
 * @return {Pool}
 */
function getDbPool() {
	// Generic DB pool, doesn't attempt to connect to MYSQL_DATABASE which may not exist yet.
	return mariadb.createPool( {
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_RW_USER,
		password: process.env.MYSQL_RW_PASS,
		port: process.env.MYSQL_PORT
	} );
}

/**
 * Clear the existing database (if any), apply the schema, and close connections.
 *
 * @param {boolean|undefined} shouldInit
 * @param {string} updateName
 * @param {string} updateFilePath
 *
 * @return {Promise<void>}
 */
function init( shouldInit, updateName, updateFilePath ) {
	updateFilePath = updateFilePath || './schema/updates.json';

	const pool = getDbPool();

	// Pool bound to MYSQL_DATABASE
	const ipoidDbPool = mariadb.createPool( {
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_RW_USER,
		password: process.env.MYSQL_RW_PASS,
		port: process.env.MYSQL_PORT,
		database: process.env.MYSQL_DATABASE
	} );

	/**
	 * Drop the MYSQL_DATABASE if it exists, and create it again.
	 *
	 * @return {Promise<void>}
	 */
	async function clearDb() {
		const connection = await pool.getConnection();
		logger.info( 'Dropping existing tables...'  );
		await connection.query( `DROP DATABASE IF EXISTS ${ process.env.MYSQL_DATABASE };` );
		await connection.query( `CREATE DATABASE ${ process.env.MYSQL_DATABASE };` );
		await connection.end();
	}

	/**
	 * Add the update_log table which needs to exist before any updates to the table can be made
	 *
	 * @return {Promise<void>}
	 */
	async function addLogger() {
		const connection = await ipoidDbPool.getConnection();
		const schema = fs.readFileSync( './schema/updates/add-update-logger.sql', 'utf8' ).toString();
		const schemaStatements = schema.split( /\n\s*\n/ );
		for ( const statement of schemaStatements ) {
			await connection.query( statement );
		}
		logger.info( 'Added update_log' );
		await connection.end();
	}

	/**
	 * Clear the existing database (if any), apply the schema, and close connections.
	 *
	 * @param {Function} resolve
	 * @return {Promise<void>}
	 */
	async function main( resolve ) {
		const schemaUpdates = JSON.parse( fs.readFileSync( updateFilePath ) ).updates;

		if ( updateName ) {
			const update = schemaUpdates.find( function ( single ) {
				return single.name === updateName;
			} );
			if ( update ) {
				const schema = fs.readFileSync( update.file, 'utf8' ).toString();
				await updateSchema( update.name, update.description, schema, ipoidDbPool );
			} else {
				logger.info( `No update found matching "${ updateName }", ignoring...` );
			}
		} else {
			if ( shouldInit || !await dbExists( getDbPool(), process.env.MYSQL_DATABASE ) ) {
				await clearDb();
				await addLogger();
			}
			for ( const update of schemaUpdates ) {
				const schema = fs.readFileSync( update.file, 'utf8' ).toString();
				await updateSchema( update.name, update.description, schema, ipoidDbPool );
			}
		}

		await pool.end();
		await ipoidDbPool.end();
		logger.info( 'Database updated!' );
		resolve();
	}

	return new Promise( ( resolve ) => {
		main( resolve );
	} );
}

module.exports = {
	init: init,
	updateSchema: updateSchema,
	dbExists: dbExists,
	getDbPool: getDbPool
};
