'use strict';

const DATADIR = process.env.DATADIR;
const { assert } = require( 'chai' );
const { promisify } = require( 'util' );
const exec = promisify( require( 'child_process' ).exec );
const initDb = require( '../../../init-db.js' );
const mariadb = require( 'mariadb' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

describe( 'shell-parameters', () => {
	it( 'should respect the --batchsize parameter', async () => {
		const connection = await mariadb.createConnection( options );

		await initDb.init( true );
		await exec( `cp ./test/data/20000101_fake.json ${ DATADIR }` );
		await exec( 'gzip ${DATADIR}/20000101_fake.json -f' );

		await exec( './main.sh --init true --today 20000101_fake --debug true' );
		const defaultBatchCountImportResult = await connection.query( 'SELECT batch_count FROM import_status LIMIT 1' );
		assert.equal( defaultBatchCountImportResult[ 0 ].batch_count, 1 );

		await exec( './main.sh --init true --today 20000101_fake --debug true --batchsize 10' );
		const smallerBatchCountImportResult = await connection.query( 'SELECT batch_count FROM import_status LIMIT 1' );
		assert.isAtLeast( smallerBatchCountImportResult[ 0 ].batch_count, 2 );

		await initDb.init( true );
		await connection.end();
	} );
} );
