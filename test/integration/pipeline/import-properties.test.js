'use strict';

const importProperties = require( '../../../import-properties.js' );
const initDb = require( '../../../init-db.js' );
const assert = require( '../../utils/assert.js' );
const mariadb = require( 'mariadb' );
const fs = require( 'fs' );

const options = {
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_RW_USER,
	password: process.env.MYSQL_RW_PASS,
	database: process.env.MYSQL_DATABASE,
	port: process.env.MYSQL_PORT
};

describe( 'import-properties', () => {
	let properties;
	before( async function () {
		properties = JSON.parse( fs.readFileSync( 'test/data/20000102_fake.properties.json' ) );
		await initDb.init( true );
		await importProperties( './test/data/20000102_fake.properties.json', 'true' );
	} );

	it( 'should populate the proxies table with the correct values', async () => {
		const connection = await mariadb.createConnection( options );
		const proxies = await connection.query( 'SELECT * FROM proxies;' );
		assert.deepEqual(
			proxies.map( ( row ) => {
				return row.proxy.toString();
			} ),
			properties.proxies
		);
		await connection.end();
	} );

	it( 'should populate the behaviors table with the correct values', async () => {
		const connection = await mariadb.createConnection( options );
		const behaviors = await connection.query( 'SELECT * FROM behaviors;' );
		assert.deepEqual(
			behaviors.map( ( row ) => {
				return row.behavior.toString();
			} ),
			properties.behaviors
		);
		await connection.end();
	} );

	it( 'should populate the tunnels table with the correct values', async () => {
		const connection = await mariadb.createConnection( options );
		const tunnels = await connection.query( 'SELECT * FROM tunnels;' );
		assert.deepEqual(
			tunnels.map( ( row ) => {
				return {
					operator: row.operator.toString(),
					type: row.type
				};
			} ),
			properties.tunnels
		);
		await connection.end();
	} );

	it( 'should accept an empty properties object', async () => {
		await initDb.init( true );
		await importProperties( './test/data/19990101_empty.properties.json', 'true' );

		const connection = await mariadb.createConnection( options );
		let proxiesCount = await connection.query( 'SELECT COUNT(*) FROM proxies;' );
		proxiesCount = Number( proxiesCount[ 0 ][ 'COUNT(*)' ] );
		let behaviorsCount = await connection.query( 'SELECT COUNT(*) FROM behaviors;' );
		behaviorsCount = Number( behaviorsCount[ 0 ][ 'COUNT(*)' ] );
		let tunnelsCount = await connection.query( 'SELECT COUNT(*) FROM behaviors;' );
		tunnelsCount = Number( tunnelsCount[ 0 ][ 'COUNT(*)' ] );
		await connection.end();

		assert.equal( proxiesCount, 0 );
		assert.equal( behaviorsCount, 0 );
		assert.equal( tunnelsCount, 0 );
	} );
} );
