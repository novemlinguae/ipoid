'use strict';

const preq   = require( 'preq' );
const assert = require( '../../../utils/assert.js' );
const Server = require( '../../../utils/server.js' );
const sinon = require( 'sinon' );
const feedUtil = require( '../../../../lib/feed-util' );

describe( 'feed v1 endpoints', function () {

	this.timeout( 20000 );

	const server = new Server();
	let stub;

	before( () => {
		stub = sinon.stub( feedUtil, 'createReadableConnection' );
		return server.start();
	} );

	after( () => {
		server.stop();
		stub.restore();
	} );

	function setupProxyAndVPNStubData() {
		stub.returns( Promise.resolve( {
			query: sinon.stub().resolves( [
				{
					label: Buffer.from( 'PROXY_1' ),
					ips: Buffer.from( '1.2.3.4,5.6.7.8' )
				},
				{
					label: Buffer.from( 'PROXY_2' ),
					ips: Buffer.from( '9.10.11.12' )
				}
			] ),
			end: sinon.stub().resolves()
		} ) );
	}

	function setupProxyAndVPNStubByLabelData() {
		stub.returns( Promise.resolve( {
			query: sinon.stub().resolves( [
				{ ip: Buffer.from( '10.9.8.7' ) },
				{ ip: Buffer.from( '6.5.4.3' ) }
			] ),
			end: sinon.stub().resolves()
		} ) );
	}

	it( 'should return proxy data for a specific label', async () => {
		setupProxyAndVPNStubByLabelData();
		const res = await preq.get( `${ server.config.uri }feed/v1/proxy/PROXY_1` );
		assert.equal( res.status, 200 );
		assert.deepEqual( res.body, [
			'10.9.8.7',
			'6.5.4.3'
		] );
	} );

	it( 'should return proxy data', async () => {
		setupProxyAndVPNStubData();
		const res = await preq.get( `${ server.config.uri }feed/v1/proxies` );
		assert.equal( res.status, 200 );
		assert.deepEqual( res.body, {
			PROXY_1: [ '1.2.3.4', '5.6.7.8' ],
			PROXY_2: [ '9.10.11.12' ]
		} );
	} );

	it( 'should return VPN data for a specific label', async () => {
		setupProxyAndVPNStubByLabelData();
		const res = await preq.get( `${ server.config.uri }feed/v1/vpn/PROXY_2` );
		assert.equal( res.status, 200 );
		assert.deepEqual( res.body, [
			'10.9.8.7',
			'6.5.4.3'
		] );
	} );

	it( 'should return VPN data', async () => {
		setupProxyAndVPNStubData();
		const res = await preq.get( `${ server.config.uri }feed/v1/vpns` );
		assert.equal( res.status, 200 );
		assert.deepEqual( res.body, {
			PROXY_1: [ '1.2.3.4', '5.6.7.8' ],
			PROXY_2: [ '9.10.11.12' ]
		} );
	} );

	it( 'should normalize IP param', async () => {
		const actorStub = sinon.stub( feedUtil, 'processActorResults' );
		actorStub.returns( Promise.resolve( {} ) );
		stub.returns( Promise.resolve( {
			query: sinon.stub().withArgs( '2001:db8:0:0:0:8a2e:370:7334' ).resolves( [ {} ] ),
			end: sinon.stub().resolves()
		} ) );
		const res = await preq.get( `${ server.config.uri }feed/v1/ip/2001:db8:0000:0:0:8a2e:370:7334` );
		assert.equal( res.status, 200 );
	} );
} );
