'use strict';

const DATADIR = process.env.DATADIR;
const getProperties = require( '../../../get-properties' );
const { expect } = require( 'chai' );
const fs = require( 'fs' );

describe( 'get-properties', () => {
	it( 'should output the correct JSON file', async () => {
		// Remove existing file to ensure we compare the output of this test run.
		fs.rmSync( `${ DATADIR }/properties.json`, {
			force: true
		} );
		await getProperties( 'test/data/20000102_fake.json' );
		const expected = JSON.parse( fs.readFileSync( 'test/data/20000102_fake.properties.json' ) );
		const actual = JSON.parse( fs.readFileSync( `${ DATADIR }/properties.json` ) );
		expect( expected ).deep.to.equal( actual );
	} );
} );
