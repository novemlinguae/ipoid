'use strict';

const { getActorTypes, getActorRisks, getTunnels, getActorBehaviors, getActorProxies } = require( '../../../import-data-utils' );
const { expect, assert } = require( 'chai' );

describe( 'import-data-utils', () => {
	describe( 'getActorBehaviors', () => {
		it( 'should return an array of valid behaviors', () => {
			const behaviors = [ null, false, '', 'valid' ];
			const result = getActorBehaviors( behaviors );
			assert.deepEqual( result, [ 'valid' ] );
		} );
	} );

	describe( 'getActorProxies', () => {
		it( 'should return an array of valid proxies', () => {
			const proxies = [ null, false, '', 'valid' ];
			const result = getActorProxies( proxies );
			assert.deepEqual( result, [ 'valid' ] );
		} );
	} );

	describe( 'getActorTypes', () => {
		it( 'should return a comma-separated string of validated types', () => {
			const types = [ 'DESKTOP', 'MOBILE', 'INVALID' ];
			const result = getActorTypes( types );
			assert.strictEqual( result, 18 );
		} );

		it( 'should return UNKNOWN if no types were passed', () => {
			const types = [];
			const result = getActorTypes( types );
			expect( result ).to.equal( 1 );
		} );
	} );

	describe( 'getActorRisks', () => {
		it( 'should return a comma-separated string of validated risks', () => {
			const risks = [ 'CALLBACK_PROXY', 'GEO_MISMATCH', 'INVALID' ];
			const result = getActorRisks( risks );
			assert.strictEqual( result, 6 );
		} );

		it( 'should return the default risk if no risks were passed', () => {
			const risks = [];
			const result = getActorRisks( risks );
			assert.strictEqual( result, 1 );
		} );
	} );

	describe( 'getTunnels', () => {
		it( 'should return an array of tunnels with associated operators', () => {
			const tunnels = [
				{ operator: 'Operator 1', type: 'VPN' },
				{ operator: 'Operator 2', type: 'PROXY' }
			];
			const result = getTunnels( tunnels );
			assert.deepEqual( result, [
				{ operator: 'Operator 1', type: 1 },
				{ operator: 'Operator 2', type: 2 }
			] );
		} );

		it( 'should return empty array if no valid tunnels were passed', () => {
			const tunnels = [ 'hi' ];
			const result = getTunnels( tunnels );
			expect( result ).to.deep.equal( [] );
		} );
	} );
} );
