'use strict';

const fs = require( 'fs' );
const readline = require( 'readline' );
const { tunnelTypes, getTunnels, getActorBehaviors, getActorProxies } = require( './import-data-utils' );
const DATADIR = process.env.DATADIR;

function init( inputFilePath ) {
	return new Promise( ( resolve ) => {
		const dataStream = readline.createInterface( {
			// Usually $DATADIR/today.json
			input: fs.createReadStream( inputFilePath ),
			crlfDelay: Infinity
		} );

		const behaviors = new Set();
		const proxies = new Set();
		const vpnTunnels = new Set();
		const proxyTunnels = new Set();

		dataStream.on( 'line', function ( line ) {
			const data = JSON.parse( line );
			if ( data.client && Array.isArray( data.client.behaviors ) ) {
				const dataBehaviors = getActorBehaviors( data.client.behaviors );
				dataBehaviors.forEach( function ( behavior ) {
					{
						behaviors.add( behavior );
					}
				} );
			}
			if ( data.client && Array.isArray( data.client.proxies ) ) {
				const dataProxies = getActorProxies( data.client.proxies );
				dataProxies.forEach( function ( proxy ) {
					proxies.add( proxy );
				} );
			}
			if ( data.tunnels && Array.isArray( data.tunnels ) ) {
				const dataTunnels = getTunnels( data.tunnels );
				dataTunnels.forEach( function ( tunnel ) {
					if ( tunnel.type === tunnelTypes.VPN ) {
						vpnTunnels.add( tunnel.operator );
					}
					if ( tunnel.type === tunnelTypes.PROXY ) {
						proxyTunnels.add( tunnel.operator );
					}
				} );
			}
		} );

		dataStream.on( 'close', () => {
			const properties = {};
			properties.behaviors = Array.from( behaviors );
			properties.proxies = Array.from( proxies );
			properties.tunnels = [
				...Array.from( vpnTunnels ).map( function ( operator ) {
					return {
						operator: operator,
						type: tunnelTypes.VPN
					};
				} ),
				...Array.from( proxyTunnels ).map( function ( operator ) {
					return {
						operator: operator,
						type: tunnelTypes.PROXY
					};
				} )
			];
			fs.writeFileSync( `${ DATADIR }/properties.json`, JSON.stringify( properties, null, '\t' ) );
			resolve();
		} );
	} );
}

module.exports = init;
