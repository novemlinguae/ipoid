#!/usr/bin/env bash

npm install
./wait-for-it.sh db:"$MYSQL_PORT" --timeout=30 --strict -- node && npm run watch
