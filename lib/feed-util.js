'use strict';

const mariadb = require( 'mariadb' );
const { actorTypes, riskTypes } = require( '../import-data-utils' );

async function createReadableConnection() {
	return await mariadb.createConnection( {
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_RO_USER,
		password: process.env.MYSQL_RO_PASS,
		database: process.env.MYSQL_DATABASE,
		port: process.env.MYSQL_PORT
	} );
}

function getLabelsFromBitmask( map, mask ) {
	return Object.keys( map ).filter( function ( label ) {
		return map[ label ] & mask;
	} );
}

/**
 * @typedef {Object} IpData
 * @property {string} ip - The IP address
 * @property {string} org - The name of the organization associated with the IP
 * @property {number} client_count - Number of clients associated with the IP.
 * @property {string[]} types - Types of devices associated with the IP (e.g. MOBILE, DESKTOP)
 * @property {string} conc_city - The city associated with the IP
 * @property {string} conc_state - The state associated with the IP
 * @property {string} conc_country - The country associated with the IP
 * @property {number} countries - Number of countries associated with the IP.
 * @property {string} location_country - The country location associated with the IP
 * @property {string[]} risks - List of risks associated with the IP address
 * @property {number} timestamp - UNIX timestamp of the time that data for this IP was last updated.
 * @property {string[]} proxies - List of proxies associated with this IP.
 * @property {string[]} behaviors - List of behaviors associated with this IP.
 * @property {string[]} tunnels - List of tunnels associated with this IP.
 */

/**
 * @param connection
 * @param actorResults
 * @return {Promise<IpData[]>}
 */
async function processActorResults( connection, actorResults ) {
	const actors = {};
	const pkids = [];
	const behaviors = {};
	const proxies = {};
	const tunnels = {};
	actorResults.forEach( function ( result ) {
		// Cache pkids returned
		pkids.push( result.pkid );
		actors[ result.pkid ] = result;
	} );

	// Get associated behavior, proxy, and tunnel ids
	const actorBehaviorIds = await connection.query(
		`SELECT * FROM actor_data_behaviors WHERE actor_data_id IN ( ${ '?,'.repeat( pkids.length ).slice( 0, -1 ) } );`,
		pkids
	);
	const actorProxyIds = await connection.query(
		`SELECT * FROM actor_data_proxies WHERE actor_data_id IN ( ${ '?,'.repeat( pkids.length ).slice( 0, -1 ) } );`,
		pkids
	);
	const actorTunnelIds = await connection.query(
		`SELECT * FROM actor_data_tunnels WHERE actor_data_id IN ( ${ '?,'.repeat( pkids.length ).slice( 0, -1 ) } );`,
		pkids
	);

	// Write behavior ids to actors and cache used behavior ids
	actorBehaviorIds.forEach( function ( actorBehavior ) {
		const actorPkid = actorBehavior.actor_data_id;
		if ( !actors[ actorPkid ].behaviors ) {
			actors[ actorPkid ].behaviors = [];
		}
		if ( !actors[ actorPkid ].behaviors.includes( actorBehavior.behavior_id ) ) {
			actors[ actorPkid ].behaviors.push( actorBehavior.behavior_id );
		}
		if ( !Object.keys( behaviors ).includes( actorBehavior.behavior_id ) ) {
			behaviors[ actorBehavior.behavior_id ] = null;
		}
	} );

	// Write proxy ids to actors and cache used proxy ids
	actorProxyIds.forEach( function ( actorProxy ) {
		const actorPkid = actorProxy.actor_data_id;
		if ( !actors[ actorPkid ].proxies ) {
			actors[ actorPkid ].proxies = [];
		}
		if ( !actors[ actorPkid ].proxies.includes( actorProxy.proxy_id ) ) {
			actors[ actorPkid ].proxies.push( actorProxy.proxy_id );
		}
		if ( !Object.keys( proxies ).includes( actorProxy.proxy_id ) ) {
			proxies[ actorProxy.proxy_id ] = null;
		}
	} );

	// Write tunnel ids to actors and cache used tunnel ids
	actorTunnelIds.forEach( function ( actorTunnel ) {
		const actorPkid = actorTunnel.actor_data_id;
		if ( !actors[ actorPkid ].tunnels ) {
			actors[ actorPkid ].tunnels = [];
		}
		if ( !actors[ actorPkid ].tunnels.includes( actorTunnel.tunnel_id ) ) {
			actors[ actorPkid ].tunnels.push( actorTunnel.tunnel_id );
		}
		if ( !Object.keys( tunnels ).includes( actorTunnel.tunnel_id ) ) {
			tunnels[ actorTunnel.tunnel_id ] = null;
		}
	} );

	let actorBehaviors = [];
	let actorProxies = [];
	let actorTunnels = [];
	// Get behavior, proxy, and tunnel data
	if ( Object.keys( behaviors ).length ) {
		actorBehaviors = await connection.query(
			`SELECT * FROM behaviors WHERE pkid IN ( ${ '?,'.repeat( Object.keys( behaviors ).length ).slice( 0, -1 ) } );`,
			Object.keys( behaviors )
		);
	}
	if ( Object.keys( proxies ).length ) {
		actorProxies = await connection.query(
			`SELECT * FROM proxies WHERE pkid IN ( ${ '?,'.repeat( Object.keys( proxies ).length ).slice( 0, -1 ) } );`,
			Object.keys( proxies )
		);
	}
	if ( Object.keys( tunnels ).length ) {
		actorTunnels = await connection.query(
			`SELECT * FROM tunnels WHERE pkid IN ( ${ '?,'.repeat( Object.keys( tunnels ).length ).slice( 0, -1 ) } );`,
			Object.keys( tunnels )
		);
	}

	// Write behavior data to the cache object, converting buffers to strings
	actorBehaviors.forEach( function ( behaviorDatum ) {
		behaviors[ behaviorDatum.pkid ] = behaviorDatum.behavior.toString();
	} );

	// Write proxy data to the cache object, converting buffers to strings
	actorProxies.forEach( function ( proxyDatum ) {
		proxies[ proxyDatum.pkid ] = proxyDatum.proxy.toString();
	} );

	// Write tunnel data to the cache object, converting buffers to strings
	actorTunnels.forEach( function ( tunnelDatum ) {
		tunnels[ tunnelDatum.pkid ] = tunnelDatum.operator.toString();
	} );

	// Write behavior, proxy, and tunnel data to actors
	Object.keys( actors ).forEach( function ( actorId ) {
		const actor = actors[ actorId ];

		const behaviorIds = actor.behaviors;
		const actorBehaviors = [];
		if ( behaviorIds ) {
			behaviorIds.forEach( function ( behaviorId ) {
				actorBehaviors.push( behaviors[ behaviorId ] );
			} );
		}
		actor.behaviors = actorBehaviors;

		const proxyIds = actor.proxies;
		const actorProxies = [];
		if ( proxyIds ) {
			proxyIds.forEach( function ( proxyId ) {
				actorProxies.push( proxies[ proxyId ] );
			} );
		}
		actor.proxies = actorProxies;

		const tunnelIds = actor.tunnels;
		const actorTunnels = [];
		if ( tunnelIds ) {
			tunnelIds.forEach( function ( tunnelId ) {
				actorTunnels.push( tunnels[ tunnelId ] );
			} );
		}
		actor.tunnels = actorTunnels;
	} );

	Object.keys( actors ).forEach( function ( actorPkid ) {
		// Stringify remaining buffers and map actors without pkids
		Object.keys( actors[ actorPkid ] ).forEach( function ( actorDatum ) {
			if ( Buffer.isBuffer( actors[ actorPkid ][ actorDatum ] ) ) {
				actors[ actorPkid ][ actorDatum ] =
					actors[ actorPkid ][ actorDatum ].toString();
			}
		} );
		// Replace values stored as bitmasks with their labels
		actors[ actorPkid ].types = getLabelsFromBitmask(
			actorTypes,
			actors[ actorPkid ].types
		);
		actors[ actorPkid ].risks = getLabelsFromBitmask(
			riskTypes,
			actors[ actorPkid ].risks
		);
		delete Object.assign( actors,
			{ [ actors[ actorPkid ].ip ]: actors[ actorPkid ] } )[ actorPkid ];
	} );

	// Close db connection and return processed data
	await connection.end();
	return actors;
}

module.exports = {
	createReadableConnection,
	processActorResults
};
