#!/usr/bin/env bash

set -e

exists() {
	[[ -f $1 ]]
}

# Make sure variables exist
debug=""
today=""
yesterday=""

# Parse CLI arguments and assign to variables, e.g. --debug true assigns $debug to true
while [ $# -gt 0 ]; do
	if [[ $1 == "--"* ]]; then
		v="${1/--/}"
		declare "$v"="$2"
		shift
	fi
	shift
done

split_and_sort() {
	rm -f "$DATADIR"/chunk*
	split -a 5 -l 1000000 "$1" "$DATADIR"/chunk
	if [ "$debug" != "true" ]; then
		rm "$1"
	fi

	if ! exists "$DATADIR"/chunk*; then
		return
	fi

	# shellcheck disable=SC2231
	for x in $DATADIR/chunk*; do
		sort "$x" > "$x".sorted
		if [ "$debug" != "true" ]; then
			rm "$x"
		fi
	done

	sort -m "$DATADIR"/chunk*.sorted > "$1.sorted"
	if [ "$debug" != "true" ]; then
		rm "$DATADIR"/chunk*.sorted
	fi
}

# Import structured logger
. logger.sh

if [ ! -f "$today" ]; then
	echo "Today's file, $today, not found" | json_logger FATAL
	exit 1
fi

echo "Removing potential stale files..." | json_logger INFO
rm -f "$DATADIR/today.json"
rm -f "$DATADIR/yesterday.json.sorted" "$DATADIR/today.json.sorted"
rm -f "$DATADIR/yesterday.unique" "$DATADIR/today.unique"
rm -f "$DATADIR/yesterday_today.unique.sorted"
rm -f "$DATADIR/remove.sql" "$DATADIR/update.sql" "$DATADIR/insert.sql"

if [ ! -f "$yesterday" ]; then
	echo "Unzipping today's file..." | json_logger INFO
	gunzip -kc "$today" > "$DATADIR/today.json"

	echo "Aggregating today's properties..." | json_logger INFO
	node -e "require('./get-properties.js')('$DATADIR/today.json');"

	echo "Outputting SQL statements..." | json_logger INFO
	node -e "require('./output-sql.js')('$DATADIR/today.json', 'import');"
else
	echo "Unzipping yesterday's file..." | json_logger INFO
	gunzip -kc "$yesterday" > "$DATADIR/yesterday.json"

	echo "Sorting yesterday's file..." | json_logger INFO
	split_and_sort "$DATADIR/yesterday.json"

	echo "Unzipping today's file..." | json_logger INFO
	gunzip -kc "$today" > "$DATADIR/today.json"

	# Future optimization: only perform on the unique lines
	# Future defense: perform on the yesterday_today file to ensure all props are present
	echo "Aggregating today's properties..." | json_logger INFO
	node -e "require('./get-properties.js')('$DATADIR/today.json');"

	echo "Sorting today's file..." | json_logger INFO
	split_and_sort "$DATADIR/today.json"

	echo "Finding lines unique to old file..." | json_logger INFO
	comm -23 "$DATADIR/yesterday.json.sorted" "$DATADIR/today.json.sorted" > "$DATADIR/yesterday.unique"

	echo "Finding lines unique to new file..." | json_logger INFO
	comm -13 "$DATADIR/yesterday.json.sorted" "$DATADIR/today.json.sorted" > "$DATADIR/today.unique"

	if [ "$debug" != "true" ]; then
		rm -f "$DATADIR/yesterday.json.sorted" "$DATADIR/today.json.sorted"
	fi

	echo "Processing unique files..." | json_logger INFO
	sed -i'' -e 's/$/@@@@@</' "$DATADIR/yesterday.unique"
	sed -i'' -e 's/$/@@@@@>/' "$DATADIR/today.unique"

	echo "Joining and sorting unique files..." | json_logger INFO
	cat "$DATADIR/yesterday.unique" "$DATADIR/today.unique" > "$DATADIR/yesterday_today.unique"

	if [ "$debug" != "true" ]; then
		rm -f "$DATADIR/yesterday.unique" "$DATADIR/today.unique"
	fi

	split_and_sort "$DATADIR/yesterday_today.unique"

	echo "Outputting SQL statements..." | json_logger INFO
	node -e "require('./output-sql.js')('$DATADIR/yesterday_today.unique.sorted');"

	if [ "$debug" != "true" ]; then
		rm -f "$DATADIR/yesterday_today.unique.sorted"
	fi
fi

if [ "$debug" != "true" ]; then
	rm -f "$DATADIR/today.json"
fi

echo "Concatenating all sql files..." | json_logger INFO
touch "$DATADIR/remove.sql" "$DATADIR/update.sql" "$DATADIR/insert.sql"
cat "$DATADIR/remove.sql" "$DATADIR/update.sql" "$DATADIR/insert.sql" > "$DATADIR/statements.sql"
if [ "$debug" != "true" ]; then
	rm -f "$DATADIR/remove.sql" "$DATADIR/update.sql" "$DATADIR/insert.sql"
fi

echo "Recording job details..." | json_logger INFO
rm -f "$DATADIR"/job.info
echo "feed_file_yesterday:$yesterday" >> "$DATADIR/job.info"
echo "feed_file_today:$today" >> "$DATADIR/job.info"
